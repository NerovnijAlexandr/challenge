import axios from "axios";
import api from '@/api.js';

export default {
    namespaced: true,
    state: {
        dogItems: [],
    },
    getters: {
        getDogItems: (state) => (text = '') => {
            const _text = text.trim();
            if(!_text) {
                return [];
            }
            return state.dogItems.filter((item) => item.startsWith(_text));
        },
    },
    mutations: {
        getDogData(state, payload) {
            state.dogItems = payload.items;
        },
    },
    actions: {
        getDogData({ commit }) {
            const url = api.dog.url;
            axios.get(url)
                .then((res) => {
                    commit('getDogData', {
                        items: Object.keys(res.data.message)
                    })
                })
                .catch((err) => {
                    console.log('dog errors', err);
                })
        }
    },
}